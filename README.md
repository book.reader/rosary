# Rosary : A Library About Rose Trees

This library is a very simple implementation of a [rose tree](https://en.wikipedia.org/wiki/Rose_tree), including higher-order functions on the rose tree to facilitate functional programming (including a depth-sensitive mapping function), and a pre-order iterator over trees.
