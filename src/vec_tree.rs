use std::{collections::VecDeque, fmt::Debug, fmt::Display, vec};

use boolinator::Boolinator;

use crate::{format::TreeFormat, RoseTree};

#[derive(Debug, Clone, Eq)]
pub enum Tree<V> {
    /// A simple (and almost certainly terribly inefficient) Rose Tree
    /// implementation.

    /// A dead end node, single value.
    Leaf(V),
    /// A branch has a head value, and then all its child values.
    Branch(V, Vec<Self>),
}

impl<V> Tree<V> {
    /// Constructors, because I don't know how to make `From` do this for me.
    pub fn leaf(val: V) -> Self {
        Tree::Leaf(val)
    }

    /// Slight misnomer, this will return a leaf in the event the iterator is
    /// empty. It's nice to have this maintain correctness, rather than exposing
    /// the tree's structure.
    pub fn branch(val: V, children: impl Iterator<Item = Self>) -> Self {
        let new_children: Vec<_> = children.into_iter().map(|c| c.into()).collect();
        if new_children.len() == 0 {
            Self::Leaf(val)
        } else {
            Self::Branch(val, new_children)
        }
    }

    /// Count the number of leaves this tree has
    pub fn num_leaves(&self) -> usize {
        match self {
            Tree::Leaf(_) => 1,
            Tree::Branch(_, b) => b.iter().fold(0, |acc, t| acc + t.num_leaves()),
        }
    }

    /// Creates a tree of the same structure as `self` where every node is a
    /// reference to the corresponding node in the owning tree.
    pub fn ref_tree(&self) -> Tree<&V> {
        match self {
            Tree::Leaf(val) => Tree::Leaf(val),
            Tree::Branch(head, children) => {
                let ref_children = children.iter().map(Tree::ref_tree).collect();
                Tree::Branch(head, ref_children)
            }
        }
    }

    /// See ref_tree, it's that but mutable.
    pub fn ref_mut_tree(&mut self) -> Tree<&mut V> {
        match self {
            Tree::Leaf(val) => Tree::Leaf(val),
            Tree::Branch(head, children) => {
                let ref_children = children.iter_mut().map(Tree::ref_mut_tree).collect();
                Tree::Branch(head, ref_children)
            }
        }
    }

    /// Add a new head to the tree, turning the current tree into the sole child
    /// of that new tree head.
    pub fn push_head(self, new_head: V) -> Self {
        Tree::Branch(new_head, vec![self])
    }

    /// Swaps out the head value of the tree with the provided one.
    pub fn swap_head(self, new_head: V) -> (V, Self) {
        match self {
            Tree::Leaf(val) => (val, Tree::Leaf(new_head)),
            Tree::Branch(head, leaves) => (head, Tree::Branch(new_head, leaves)),
        }
    }

    /// Destructure the tree into references if you don't care about the variant
    /// that it is.
    pub fn destructure_ref(&self) -> (&V, Vec<Tree<&V>>) {
        match self {
            Tree::Leaf(val) => (val, vec![]),
            Tree::Branch(head, leaves) => (head, leaves.iter().map(Self::ref_tree).collect()),
        }
    }

    /// As destructure_ref, but mutably!
    pub fn destructure_ref_mut(&mut self) -> (&mut V, Vec<Tree<&mut V>>) {
        match self {
            Tree::Leaf(val) => (val, vec![]),
            Tree::Branch(head, leaves) => {
                (head, leaves.iter_mut().map(Self::ref_mut_tree).collect())
            }
        }
    }

    // Maps a function over just the head of the tree.
    pub fn map_head(self, mut head_fn: impl FnMut(V) -> V) -> Self {
        match self {
            Tree::Leaf(val) => Tree::Leaf(head_fn(val)),
            Tree::Branch(head, children) => Tree::Branch(head_fn(head), children),
        }
    }

    fn map_leaves_impl(self, mut child_fn: &mut impl FnMut(V) -> V) -> Self {
        match self {
            Tree::Leaf(v) => child_fn(v).into(),
            Tree::Branch(head, children) => Tree::Branch(
                head,
                children
                    .into_iter()
                    .map(|child| child.map_leaves(&mut child_fn))
                    .collect(),
            ),
        }
    }

    /// Map over only the leaves of the tree.
    pub fn map_leaves(self, mut child_fn: impl FnMut(V) -> V) -> Self {
        self.map_leaves_impl(&mut child_fn)
    }

    fn map_impl<U, F: FnMut(&V) -> U>(&self, func: &mut F) -> Tree<U> {
        match self {
            Tree::Leaf(val) => Tree::Leaf(func(val)),
            Tree::Branch(head, children) => {
                let head = func(head);
                let mut new_children = Vec::with_capacity(children.len());
                for child in children {
                    new_children.push(child.map_impl(func))
                }

                Tree::Branch(head, new_children)
            }
        }
    }

    /// Map over the whole tree.
    pub fn map<U>(&self, mut func: impl FnMut(&V) -> U) -> Tree<U> {
        self.map_impl(&mut func)
    }

    fn map_into_impl<U>(self, mut func: &mut impl FnMut(V) -> U) -> Tree<U> {
        match self {
            Tree::Leaf(val) => Tree::Leaf(func(val)),
            Tree::Branch(head, children) => Tree::Branch(
                func(head),
                children
                    .into_iter()
                    .map(|child| child.map_into(&mut func))
                    .collect(),
            ),
        }
    }

    /// Map over the whole tree, consuming it in the process.
    pub fn map_into<U>(self, mut func: impl FnMut(V) -> U) -> Tree<U> {
        self.map_into_impl(&mut func)
    }

    fn try_map_impl<U, E>(self, func: &mut impl FnMut(V) -> Result<U, E>) -> Result<Tree<U>, E> {
        match self {
            Tree::Leaf(l) => func(l).map(Tree::Leaf),
            Tree::Branch(h, s) => {
                let h = func(h)?;
                s.into_iter()
                    .map(|t| t.try_map_impl(func))
                    .collect::<Result<_, _>>()
                    .map(|s| Tree::Branch(h, s))
            }
        }
    }

    /// Map a fallible function over the tree, stopping at the first error,
    /// executing first on the head node, and then on each of its children in
    /// order.
    pub fn try_map<U, E>(self, mut func: impl FnMut(V) -> Result<U, E>) -> Result<Tree<U>, E> {
        self.try_map_impl(&mut func)
    }

    /// Maps over the tree, where each child of the same parent receives the same
    /// accumulator value as a function argument.
    /// Add more variants of this for different argument mutabilities maybe?
    pub fn depth_map<A: Clone, U>(&self, initial: A, func: &impl Fn(A, &V) -> (A, U)) -> Tree<U> {
        match self {
            Tree::Leaf(val) => {
                let (_, new_val) = func(initial, val);
                Tree::Leaf(new_val)
            }
            Tree::Branch(head, children) => {
                let (accum, new_head) = func(initial, head);
                let mut new_children = Vec::with_capacity(children.len());
                for child in children {
                    new_children.push(child.depth_map(accum.clone(), func))
                }
                Tree::Branch(new_head, new_children)
            }
        }
    }

    /// Removes all values from the tree which don't satisfy the predicate. In the
    /// case of branches, if the head of the branch does not satisfy the
    /// predicate, both it and all its children are removed.
    pub fn filter(self, func: &impl Fn(&V) -> bool) -> Option<Self> {
        match self {
            Tree::Leaf(val) => func(&val).as_some(Tree::Leaf(val)),
            Tree::Branch(head, children) => func(&head).as_some_from(|| {
                Tree::Branch(
                    head,
                    children
                        .into_iter()
                        .filter_map(|c| c.filter(func))
                        .collect(),
                )
            }),
        }
    }

    /// Generates a pre-order traversal of the tree.
    pub fn preorder_iter<'slef>(&'slef self) -> PreorderRoseIter<V> {
        let mut stack = VecDeque::new();
        stack.push_back(self);
        PreorderRoseIter { stack }
    }
}

impl<V> RoseTree<V> for Tree<V> {
    fn is_leaf(&self) -> bool {
        match self {
            Tree::Leaf(_) => true,
            _ => false,
        }
    }

    fn is_branch(&self) -> bool {
        match self {
            Tree::Branch(_, _) => true,
            _ => false,
        }
    }

    fn size(&self) -> usize {
        match self {
            Tree::Leaf(_) => 1,
            Tree::Branch(_, children) => 1 + children.iter().map(|c| c.size()).sum::<usize>(),
        }
    }

    fn add_child(self, child: V) -> Self {
        match self {
            Self::Leaf(l) => Self::Branch(l, vec![child.into()]),
            Self::Branch(h, mut cs) => {
                cs.push(child.into());
                Self::Branch(h, cs)
            }
        }
    }

    fn destructure(self) -> (V, Vec<Self>) {
        match self {
            Tree::Leaf(val) => (val, vec![]),
            Tree::Branch(head, leaves) => (head, leaves),
        }
    }
}

impl<V> From<V> for Tree<V> {
    /// Convenience function to make wrapping anything in a leaf nicer.
    fn from(val: V) -> Self {
        Tree::Leaf(val)
    }
}

impl<V: Display> Display for Tree<V> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        write!(f, "{}", self.fmt_tree())
    }
}

impl<V, I: IntoIterator<Item = V>> From<(V, I)> for Tree<V> {
    /// Convenience to make wrapping anything in a Branch nicer(I know, shocking).
    fn from((head, child_collection): (V, I)) -> Tree<V> {
        let children = child_collection.into_iter().map(Into::into).collect();
        Tree::Branch(head, children)
    }
}

impl<V: PartialEq> PartialEq for Tree<V> {
    /// It'd be really nice to be able to check equality of variants
    /// independently of the values within the variants, but I have no idea how
    /// or if that's possible.
    fn eq(&self, other: &Self) -> bool {
        match self {
            Tree::Leaf(vl) => match other {
                Tree::Leaf(vr) => vl == vr,
                _ => false,
            },
            Tree::Branch(head, children) => match other {
                Tree::Branch(other_head, other_children) => {
                    if head != other_head {
                        return false;
                    };
                    if children.len() != other_children.len() {
                        return false;
                    };
                    children
                        .iter()
                        .zip(other_children.iter())
                        .map(|(child, other_child)| child == other_child)
                        .fold(true, |accum, is_equal| accum && is_equal)
                }
                _ => false,
            },
        }
    }
}

/// A depth-first iterator over references to the tree values. No points for
/// guessing the order of the returned items.
pub struct PreorderRoseIter<'slef, T> {
    stack: VecDeque<&'slef Tree<T>>,
}

impl<'slef, T> Iterator for PreorderRoseIter<'slef, T> {
    type Item = &'slef T;

    fn next(&mut self) -> Option<Self::Item> {
        let next_node = self.stack.pop_front()?;
        match next_node {
            Tree::Leaf(ref val) => Some(val),
            Tree::Branch(ref head, ref children) => {
                for child in children.iter().rev() {
                    self.stack.push_front(child);
                }
                Some(head)
            }
        }
    }
}

#[cfg(test)]
mod test {
    use crate::vec_tree::Tree;

    fn test_tree() -> Tree<usize> {
        Tree::Branch(
            1,
            vec![
                Tree::Branch(2, vec![Tree::Leaf(3), Tree::Leaf(4), Tree::Leaf(5)]),
                Tree::Branch(6, vec![Tree::Leaf(7), Tree::Leaf(8), Tree::Leaf(9)]),
                Tree::Branch(10, vec![Tree::Leaf(11), Tree::Leaf(12), Tree::Leaf(13)]),
            ],
        )
    }

    #[test]
    fn test_preorder_actually_preordered() {
        let expected: Vec<usize> = vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
        let actual: Vec<usize> = test_tree().preorder_iter().map(|v| *v).collect();
        assert_eq!(expected, actual);
    }
}
