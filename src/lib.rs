extern crate boolinator;

pub mod format;
pub mod vec_tree;

pub trait RoseTree<V>: Sized {
    fn is_leaf(&self) -> bool;
    fn is_branch(&self) -> bool;
    /// Calculates the number of nodes in the tree.
    fn size(&self) -> usize;

    /// Pull a tree apart into its head node, and an iterator over any children.
    fn destructure(self) -> (V, Vec<Self>);

    /// Adds the passed value as a child of `self` to the structure.
    fn add_child(self, child: V) -> Self;
}
