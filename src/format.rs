use crate::vec_tree::Tree;
use std::{fmt::Display, iter::repeat};

#[derive(Clone, Copy, Debug)]
pub struct Indenter {
    /// Indenter Docs
    /// ╠═Controls how to render tree-format data
    /// ╠═Character varieties
    /// ║ ╠═left_bar <║>
    /// ║ ╠═inward_branch <╠>
    /// ║ ╠═horizontal_bar <═>
    /// ║ ╚═last_entry <╚>
    /// ╠═Indenting parameters
    /// ║ ╠═current_level
    /// ║ ║ ╚═Tracks which indent level we're on. This sentence is on level 3!
    /// ║ ╚═level_width
    /// ║   ╚═States how wide each level's decorations should be.
    /// ╚═This takes TreeFormat types and renders them as pretty-printed trees. It
    /// does not currently cope with wraparound (oops).
    current_level: usize,
    level_width: usize,
    vertical_bar: char,
    inward_branch: char,
    horizontal_bar: char,
    last_entry: char,
}

/// An object used by the TreeFormat to control and track formatting settings.
impl Indenter {
    pub fn next(&self) -> Indenter {
        Indenter {
            current_level: self.current_level + 1,
            ..*self
        }
    }

    /// How long the header of a line should be for this indenter.
    pub fn header_len(&self) -> usize {
        self.current_level * self.level_width
    }

    /// Generate the text of the header for the line.
    pub fn line_header(&self, arms_to_continue: &Vec<bool>) -> String {
        let mut accum = String::with_capacity(self.header_len());
        let mut arm_iter = arms_to_continue.iter();
        let is_last_line = if let Some(arm) = arm_iter.next_back() {
            !arm
        } else {
            return String::with_capacity(0);
        };

        for arm in arm_iter {
            // Add bridging bar, if needed.
            if *arm {
                accum.push(self.vertical_bar)
            } else {
                accum.push(' ')
            };
            // Adding the horizontal spacing.
            accum.extend(repeat(' ').take(self.level_width - 1));
        }

        let bar = if is_last_line {
            self.last_entry
        } else {
            self.inward_branch
        };
        accum.push(bar);
        accum.extend(repeat(self.horizontal_bar).take(self.level_width - 1));
        accum
    }

    /// Pads a string with the header, toggling branches based on the arms outside
    /// of the current layer.
    pub fn pad_string(&self, continue_arms: &Vec<bool>, text: &String) -> String {
        format!("{}{}", self.line_header(continue_arms), text)
    }

    /// Pads every string in a RoseVecTree, producing a new, padded RoseVecTree.
    pub fn pad_tree(&self, tree: &Tree<String>) -> Tree<String> {
        fn rec_pad(
            indenter: &Indenter,
            tree: &Tree<String>,
            continue_arms: &mut Vec<bool>,
        ) -> Tree<String> {
            match tree {
                //RoseVecTree::Empty => RoseVecTree::Empty,
                Tree::Leaf(val) => Tree::Leaf(indenter.pad_string(continue_arms, val)),
                Tree::Branch(head, children) => {
                    let new_head = indenter.pad_string(continue_arms, head);

                    let mut child_iter = children.iter();
                    let last_child = if let Some(child) = child_iter.next_back() {
                        child
                    } else {
                        return Tree::Leaf(new_head);
                    };
                    let child_indenter = indenter.next();
                    let mut new_children = Vec::with_capacity(children.len());

                    continue_arms.push(true);
                    for child in child_iter {
                        new_children.push(rec_pad(&child_indenter, child, continue_arms));
                    }
                    continue_arms.pop();

                    continue_arms.push(false);
                    new_children.push(rec_pad(&child_indenter, last_child, continue_arms));
                    continue_arms.pop();
                    Tree::Branch(new_head, new_children)
                }
            }
        }
        rec_pad(self, tree, &mut Vec::with_capacity(self.current_level))
    }
}

/// Very convenient, I'm not copy-pasting the fancy characters every single
/// time.
impl Default for Indenter {
    fn default() -> Self {
        Self {
            current_level: 0,
            level_width: 2,
            horizontal_bar: '═',
            inward_branch: '╠',
            last_entry: '╚',
            vertical_bar: '║',
        }
    }
}

/// A trait which allows implementers to specify how they should be printed as
/// a tree.
pub trait TreeFormat {
    /// The only thing you need to define is the conversion from your type to a
    /// RoseVecTree.
    fn treeify(&self) -> Tree<String>;

    /// Indent adds the decorations to the strings of the tree with the default
    /// formatter.
    fn indent(&self) -> Tree<String> {
        Indenter::default().pad_tree(&self.treeify())
    }

    /// Actually converts the tree into a single string for printing.
    fn fmt_tree(&self) -> String {
        self.indent()
            .preorder_iter()
            .fold(String::new(), |accum, line| accum + "\n" + line)
    }
}

/// If you're a RoseVecTree, you get TreeFormat for free so long as T can be
/// printed. If printing T involves printing newlines, don't. It'll get weird.
impl<T: Display> TreeFormat for Tree<T> {
    fn treeify(&self) -> Tree<String> {
        self.map(|val| val.to_string())
    }
}

impl Display for dyn TreeFormat {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.fmt_tree())
    }
}

#[cfg(test)]
mod test {
    use super::Indenter;
    use crate::{format::TreeFormat, vec_tree::Tree};

    #[test]
    fn indent_levels() {
        let indenter = Indenter::default();
        assert_eq!(indenter.line_header(&vec![]), "", "{:#?}", indenter);
        assert_eq!(indenter.line_header(&vec![]), "", "{:#?}", indenter);

        let indenter = indenter.next();
        assert_eq!(indenter.line_header(&vec![true]), "╠═", "{:#?}", indenter);
        assert_eq!(indenter.line_header(&vec![false]), "╚═", "{:#?}", indenter);

        let indenter = indenter.next();
        assert_eq!(
            indenter.line_header(&vec![true, true]),
            "║ ╠═",
            "{:#?}",
            indenter
        );
        assert_eq!(
            indenter.line_header(&vec![true, false]),
            "║ ╚═",
            "{:#?}",
            indenter
        );
    }

    #[test]
    fn skip_level() {
        let _indenter = Indenter::default();
        let doc_tree = Tree::Branch(
            "Level 0",
            vec![
                Tree::Branch(
                    "Level 1",
                    vec![Tree::Branch("Level 2", vec![Tree::Leaf("Level 3")])],
                ),
                Tree::Leaf("Bottom"),
            ],
        );
        assert_eq!(
            r#"
Level 0
╠═Level 1
║ ╚═Level 2
║   ╚═Level 3
╚═Bottom"#,
            doc_tree.fmt_tree()
        );
    }

    #[test]
    fn print_the_docs() {
        let doc_tree = Tree::Branch(
      "Indenter Docs",
      vec! [
        Tree::Leaf("Controls how to render tree-format data"),
        Tree::Branch(
          "Character varieties",
          vec! [
            Tree::Leaf("left_bar <║>"),
            Tree::Leaf("inward_branch <╠>"),
            Tree::Leaf("horizontal_bar <═>"),
            Tree::Leaf("last_entry <╚>")]),
        Tree::Branch(
          "Indenting parameters",
          vec! [
            Tree::Branch(
              "current_level",
              vec! [
                Tree::Leaf("Tracks which indent level we're on. This sentence is on level 3!")]),
            Tree::Branch(
              "level_width",
              vec! [
                Tree::Leaf("States how wide each level's decorations should be.")]),
        ]),
      Tree::Leaf("This takes TreeFormat types and renders them as pretty-printed trees. It does not currently cope with wraparound (oops).")]);

        println!("{}", doc_tree.fmt_tree())
    }
}
